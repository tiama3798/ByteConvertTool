﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ByteConvertTool
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        const int ThisDecimal = 2;
        //比特
        private void txtb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ChangeTxtB) //如果当前是正在修改txtB，后面的代码不执行
                return;
            try
            {
                if (string.IsNullOrEmpty(txtb.Text))
                {
                    txtb.Focus();
                    return;
                }

                double number = double.Parse(txtb.Text);
                txtB.Text = (number / 8.0).ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        //标识txtB对话框正在修改数据
        private bool ChangeTxtB = false;
        //字节
        private void txtB_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            label1.Content = "";
            try
            {
                if (string.IsNullOrEmpty(txtB.Text))
                {
                    txtB.Focus();
                    return;
                }
                //标识正在修改txtB
                ChangeTxtB = true;
                //吉字节变换是统一修改其他内容
                double number = double.Parse(txtB.Text);
                txtb.Text = (number * 8).ToString();//当修改Text属性时，txtb_TextChanged会立即同步触发
                txtKB.Text = (number / 1024.0).ToString();
                txtMB.Text = (number / 1024.0 / 1024.0).ToString();
                txtGB.Text = (number / 1024.0 / 1024.0 / 1024.0).ToString();
                txtTB.Text = (number / 1024.0 / 1024.0 / 1024.0 / 1024.0).ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
            //标识结束修改txtB
            ChangeTxtB = false;
        }
        //千字节
        private void txtKB_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ChangeTxtB)
                return;
            try
            {
                if (string.IsNullOrEmpty(txtKB.Text))
                {
                    txtKB.Focus();
                    return;
                }

                double number = double.Parse(txtKB.Text);
                txtB.Text = (number * 1024).ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        //兆字节
        private void txtMB_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ChangeTxtB)
                return;
            try
            {
                if (string.IsNullOrEmpty(txtMB.Text))
                {
                    txtMB.Focus();
                    return;
                }

                double number = double.Parse(txtMB.Text);
                txtB.Text = (number * 1024 * 1024).ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        //吉字节
        private void txtGB_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ChangeTxtB)
                return;
            try
            {
                if (string.IsNullOrEmpty(txtGB.Text))
                {
                    txtGB.Focus();
                    return;
                }

                double number = double.Parse(txtGB.Text);
                txtB.Text = (number * 1024 * 1024 * 1024).ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        //太字节
        private void txtTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (ChangeTxtB)
                return;
            try
            {
                if (string.IsNullOrEmpty(txtTB.Text))
                {
                    txtTB.Focus();
                    return;
                }

                double number = double.Parse(txtTB.Text);
                txtB.Text = (number * 1024 * 1024 * 1024 * 1024).ToString();
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
            }
        }
        private void ShowError(string str)
        {
            //  MessageBox.Show(str);
            label1.Content = str;

        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
    }
}
